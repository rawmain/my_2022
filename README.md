# Some (_quick_) analysis notes about MY 2022
**_Last Updated :_**
- **_April 30th, 2022_**    - _added notice about MY2022 app shutdown (but app services are still online)_
- **_March 7th, 2022_**     - _added new release 2.0.9 build AAB Bundle available from Google PlayStore_
- **_March 3rd, 2022_**     - _added new release 2.0.9 build Universal APK (few bugfixes)_
- **_February 17th, 2022_** - _added Appendix II (further comparison notes about **ALL past 2.0.x releases**)_
- **_February  8th, 2022_** - _updated the build version available from Samsung Store_
- **_February  5th, 2022_** - _added Appendix (**comparison between current & past builds**)_

\
**My 2022** is a mobile service application platform providing information service of the Olympic and Paralympic Winter Games Beijing 2022 (the Beijing 2022 Games).

In the past few days I have read 2 third-party reports about this application.

| Author | Report Link |
|--------|-------------|
| [Citizen Lab](https://twitter.com/citizenlab/status/1483379681206415361) | https://citizenlab.ca/2022/01/cross-country-exposure-analysis-my2022-olympics-app/ |
| | **Citizen Lab** has examined MY 2022 versions for iOS up to release 2.0.5 (published on January 17th) and mainly focuses on related issues about content auditing and censorship. Some findings of Citizen Lab report also apply to current versions of MY 2022 for Android, such as the high interception risk of network traffic (_owing to SSL broken implementation & even HTTP-only connections with some endpoints_). |
| [Jonathan Scott](https://twitter.com/jonathandata1/status/1485208193273896964) | https://github.com/jonathandata1/2022_beijing |
| | **Jonathan Scott** has examined MY 2022 versions up to release 2.0.4 for Android and release 2.0.5 for iOS, mainly focusing on the iOS versions. However, his report seems more like an abandoned WIP, rathen than a fulfilled analysis. Besides, it's even quite controversial = no runtime evidences to support his claims about data exfiltration and even (rookie) mistakes in his static code analysis. Leaving out the wrong conclusions, he has provided anyway few useful hints for a deeper analysis of the app. |

The release 2.0.7 of MY 2022 has just been published on January 28th... and 3 days after there is already a new one = release 2.0.8.

Therefore, since such reports weren't anyway focused on the Android versions, first I've taken a (_quick_) look at MY 2022 releases 2.0.7 & 2.0.8 for Android (_chapters from [1](#1-lets-download-the-latest-android-releases-of-my-2022-app) to [6](#6-conclusions)_).

Then, since MY 2022 2.0.x official packages are still available for download from the official sources, I've even compared such current/recent releases with the past ones - going up back to 2.0.0 (_[Appendix](#7-appendix) & [Appendix II](#8-appendix-ii)_)

----------------

### Index

- 1. [Let's download the latest Android release(s) of MY 2022 app](#1-lets-download-the-latest-android-releases-of-my-2022-app)

- - 1.1 [Which release should I download?](#11-which-release-should-i-download)

- - 1.2 [Changelog](#12-changelog)

- 2. [Some analysis tips](#2-some-analysis-tips)

- 3. [Let's see now what we've just downloaded](#3-lets-see-now-what-weve-just-downloaded)

- 4. [Start me up!](#4-start-me-up)

- 5. [Privacy Matters & Data Security](#5-privacy-matters-data-security)

- - 5.1. [Permissions](#51-permissions)

- - 5.2. [Device Info & Tracking](#52-device-info-tracking)

- 6. [Conclusions](#6-conclusions)

- 7. [Appendix](#7-appendix)

- 8. [Appendix II](#8-appendix-ii)

-----------------

## 1. Let's download the latest Android release(s) of MY 2022 app

### 1.1. Which release should I download?

When - at the end of January - I've started writing these notes there were officially available for download :

- 2 official builds (Universal APK & Bundle AAB) of MY 2022 release 2.0.8 for Android

- 2 official builds (Universal APK & Bundle AAB) of MY 2022 release 2.0.7 for Android

Since March 1st the Universal APK packages have been updated to release 2.0.9 builds (_few bugfixes_).

Since March 6th the AAB Bundle package has been updated to release 2.0.9 build (_few bugfixes & removal on unused permissions_).

These are the builds' references.

| Build Version     | Release | Package Deployment                              |
|-------------------|---------|-------------------------------------------------|
| #2122096155       | 2.0.9   | Universal APK for armeabi-v7a / arm64-v8a arch  |
| #22030518         | 2.0.9   | Bundle AAB for armeabi-v7a / arm64-v8a arch     |
| #2122061137       | 2.0.8   | Universal APK for armeabi-v7a / arm64-v8a arch  | 
| #22020118         | 2.0.8   | Bundle AAB for armeabi-v7a / arm64-v8a arch     |
| #2122054203       | 2.0.7   | Universal APK for armeabi-v7a / arm64-v8a arch  |
| #22012720         | 2.0.7   | Bundle AAB for armeabi-v7a / arm64-v8a arch     |

\
These are the main official download sources together with the references of their latest available release builds.

> _Once the Paralympics ended (on March 13th 2022) and the foreign teams returned to their countries, the MY2022 app has been discontinued._
>
> _**Since April MY2022 is NO longer available for download from the official sources.**_
>
> _Backup APK packages are available from :_
> - [samples](samples) : MY2022 releases 2.0.7/2.0.8/2.0.9
>
> - [appendix](appendix) : MY2022 releases 2.0.0/2.0.1/2.0.2/2.0.3/2.0.4/2.0.6 

| Source       | Release | Build       | Link                                                                            |
|--------------|---------|-------------|---------------------------------------------------------------------------------|
| Beijing 2022 | 2.0.9   | #2122096155 | https://dongaostatic.beijing2022.cn/download/                                   |
|              |         |             | https://dongaostatic.beijing2022.cn/android/dongao.apk (_direct_)               |
|              |         |             |                                                                                 |
| AppGallery   | 2.0.9   | #2122096155 | https://appgallery.huawei.com/app/C103999097                                    |
|              |         |             |                                                                                 |
| Tencent QQ   | 2.0.9   | #2122096155 | https://a.app.qq.com/o/simple.jsp?pkgname=com.systoon.dongaotoon                |
|              |         |             | http://imtt.dd.qq.com/16891/apk/D7DD6C1E1F75F76A4B640A55E2595537.apk (_direct_) |
|              |         |             |                                                                                 |
| Samsung      | 2.0.9   | #2122096155 | https://galaxystore.samsung.com/detail/com.systoon.dongaotoon                   |
|              |         |             |                                                                                 |
| Play Store   | 2.0.9   | #22030518   | https://play.google.com/store/apps/details?id=com.systoon.dongaotoon            |
     

As you can see :

- Release 2.0.9 - build #2122096155 is available from Beijing 2022 Games official website + Huawei / Tencent / Samsung

- Release 2.0.9 - build #22030518 is only available from Google Play Store

- Release 2.0.8 - build #2122061137 was available from Beijing 2022 Games official website + Huawei / Tencent / Samsung until March 1st

- Release 2.0.8 - build #22020118 was only available from Google Play Store until March 6th

- Release 2.0.7 - build #2122054203 - although superseded - was still available for download from Samsung until February 7th

\
Both Universal APK and AAB 2.0.7 builds have the same app code & signatures, just (_obviously_) different appID references - according to the different store/source requirements for published apps. 

There are indeed no real code/activities' differences between those 2.0.7 builds. The release 2.0.7 build available on the Google Play Store only uses slightly different settings for the packer (_SecNeo Apkwrapper_) libraries processes - just because of the split-APKs deployment.

\
**Since the release of 2.0.8 builds there are instead some differences instead between the Universal APK & AAB Bundle packages.**

**Release 2.0.8 Build #22020118 (AAB Bundle package - only available from Play Store) included indeed some bugfixes and small privacy-related improvements** over Release 2.0.8 Build #2122061137.

**Further details in the next chapters.**

\
Release 2.0.9 build #2122096155 is just release 2.0.8 Build #2122061137 with few bugfixes. No functional additions or removals.

Release 2.0.9 build #22030518 is just release 2.0.8 Build #22020118 with few bugfixes + removal of unused permissions (_Bluetooth and NFC_).

> _Side note :_ 
>
> Some users won't find anyway the MY 2022 app among the Google Play Store search results. That's because of the download restrictions' settings, applied by the publisher, in order to prevent any device - _not detected as a [Google Play Certified](https://storage.googleapis.com/play_public/supported_devices.html) one_ - from installing the app.
>
> The publisher has indeed applied some measures in order to slow down the in-depth analysis of the app (_further details in the next paragraph_), including the download-restriction for those devices with uncertified / unofficial firmware on board.
>
> However, this is a (very) weak measure. Not only because the other official sources aren't applying any download-restriction, but also because it can be easily bypassed. No need even to edit build-props/fingerprint, since users can still retrieve the official split-APKs package from another device & install it without any issue.
>
> **Since March 20th 2022 the MY2022 app is NO longer available from the Google Play Store**. Split-APKs ARM64-v8a packages of releases 2.0.7/2.0.8/2.0.9 are available among the [collected samples](samples).
>
> | _Play Store restriction warning_                | _Split-APKs manual setup_                   |
> |-------------------------------------------------|---------------------------------------------|
> | ![alt text](doc_img/img00.png "DL Restriction") | ![alt text](doc_img/img01B.png "Split-APKs") |

Since I've also uploaded the [collected samples](samples) to online scanner engines, these are some online-analysis references.

- **Universal APK 2.0.9 #2122096155** - w/o armeabi-v7a libs (_bc upload limits_)

| Analysis | Link |
|----------|------|
| JoeSandbox (ARM64-only) | https://www.joesandbox.com/analysis/582348/0/html   |

- **Universal APK 2.0.9 #2122096155** - Beijing 2022 full package

| Analysis | Link |
|----------|------|
| VirusTotal | https://www.virustotal.com/gui/file/b3467d5db1c29492b7ea17ea886e76f96620a801ba809d111f79d4ae3698437b   |
| Apklab     | https://apklab.io/apk.html?hash=b3467d5db1c29492b7ea17ea886e76f96620a801ba809d111f79d4ae3698437b   |

- **Universal APK 2.0.9 #2122096155** - Huawei AppGallery full package

| Analysis | Link |
|----------|------|
| VirusTotal | https://www.virustotal.com/gui/file/c3b456967593792771a12d9181251f8901252b008f8abb962279a5ae6114ee60   |
| Apklab     | https://apklab.io/apk.html?hash=c3b456967593792771a12d9181251f8901252b008f8abb962279a5ae6114ee60   |

- **Universal APK 2.0.9 #2122096155** - Tencent full package

| Analysis | Link |
|----------|------|
| VirusTotal | https://www.virustotal.com/gui/file/85608a0b55b4552b4d1084df29786110f97f8157a3f7daf89ef9665ad094754c   |
| Apklab     | https://apklab.io/apk.html?hash=85608a0b55b4552b4d1084df29786110f97f8157a3f7daf89ef9665ad094754c   |

- **Bundle 2.0.9 #22030518** - Google PlayStore base.apk

| Analysis | Link |
|----------|------|
| VirusTotal | https://www.virustotal.com/gui/file/927cb3f4361e831632682200df72efdfb8ebfde34717fb257890ea4e5d15be99   |
| Apklab     | https://apklab.io/apk.html?hash=927cb3f4361e831632682200df72efdfb8ebfde34717fb257890ea4e5d15be99   |

\
.
---------------------

- **Universal APK 2.0.8 #2122061137** - w/o armeabi-v7a libs (_bc upload limits_)

| Analysis | Link |
|----------|------|
| Pithus (ARM64-only)     | https://beta.pithus.org/report/f9b6cb67a92787892c67ae06d588cef955b9bc54f31e6293c0f265ac7d0cb522   |
| JoeSandbox (ARM64-only) | https://www.joesandbox.com/analysis/564312/0/html   |

- **Universal APK 2.0.8 #2122061137** - Beijing 2022 full package

| Analysis | Link |
|----------|------|
| VirusTotal | https://www.virustotal.com/gui/file/65c12376322ce9854afdfce07536ca8772cee8c03c6a85d55fa7f8de5c52f664   |
| Apklab     | https://apklab.io/apk.html?hash=65c12376322ce9854afdfce07536ca8772cee8c03c6a85d55fa7f8de5c52f664   |

- **Universal APK 2.0.8 #2122061137** - Huawei AppGallery full package

| Analysis | Link |
|----------|------|
| VirusTotal | https://www.virustotal.com/gui/file/225cfa213b001a6722e4f7e75603cb13dec02478389b76382cee8d9211efd2c0   |
| Apklab     | https://apklab.io/apk.html?hash=225cfa213b001a6722e4f7e75603cb13dec02478389b76382cee8d9211efd2c0   |

- **Universal APK 2.0.8 #2122061137** - Tencent full package

| Analysis | Link |
|----------|------|
| VirusTotal | https://www.virustotal.com/gui/file/f170b46d0e5472722e5b9942e58aab5d5ea9470185916d9c35d9388c41f71a5b   |
| Apklab     | https://apklab.io/apk.html?hash=f170b46d0e5472722e5b9942e58aab5d5ea9470185916d9c35d9388c41f71a5b   |

- **Bundle 2.0.8 #22020118** - Google PlayStore base.apk

| Analysis | Link |
|----------|------|
| VirusTotal | https://www.virustotal.com/gui/file/43442048ba09edb47daa76b537a61f3eb54953edd738c9deb06cd4014363189b   |
| Apklab     | https://apklab.io/apk.html?hash=43442048ba09edb47daa76b537a61f3eb54953edd738c9deb06cd4014363189b   |

\
.
-------------------------------

- **Universal APK 2.0.7 #2122054203** - w/o arm64-v8a libs (_bc upload limits_)

| Analysis | Link |
|----------|------|
| Pithus (ARMv7-only)     | https://beta.pithus.org/report/d5462ccd3bc9e66270c38cf1cfc8d683e26154966cbd4b9e82b822458396167b   |
| JoeSandbox (ARMv7-only) | https://www.joesandbox.com/analysis/562596/0/html   |

- **Universal APK 2.0.7 #2122054203** - Beijing 2022 full package

| Analysis | Link |
|----------|------|
| VirusTotal | https://www.virustotal.com/gui/file/815d8f8ab14bd3374f82f000d909d318cc95657489f065313c9185bcdfb56020   |
| Apklab     | https://apklab.io/apk.html?hash=815d8f8ab14bd3374f82f000d909d318cc95657489f065313c9185bcdfb56020   |


- **Universal APK 2.0.7 #2122054203** - Huawei AppGallery full package

| Analysis | Link |
|----------|------|
| VirusTotal | https://www.virustotal.com/gui/file/f4c99d11a375e556579342dd7dac61499e88f7541bd5f4a2e57f5d2a84a89fcd   |
| Apklab     | https://apklab.io/apk.html?hash=f4c99d11a375e556579342dd7dac61499e88f7541bd5f4a2e57f5d2a84a89fcd   |

- **Universal APK 2.0.7 #2122054203** - Tencent full package

| Analysis | Link |
|----------|------|
| VirusTotal | https://www.virustotal.com/gui/file/9d421d883c695c41bfff3cc7ae35d5de047e3c83057ce607b037fb390f6949c2   |
| Apklab     | https://apklab.io/apk.html?hash=9d421d883c695c41bfff3cc7ae35d5de047e3c83057ce607b037fb390f6949c2   |

- **Bundle 2.0.7 #22012720** - Google PlayStore base.apk

| Analysis | Link |
|----------|------|
| VirusTotal | https://www.virustotal.com/gui/file/d93945eb76beb24c3c03ee94ec25168e1d8a4b89caa81567dd3e4c7739bb1d0a   |
| Apklab     | https://apklab.io/apk.html?hash=d93945eb76beb24c3c03ee94ec25168e1d8a4b89caa81567dd3e4c7739bb1d0a   |

- **Bundle 2.0.7 #22012720** - Google PlayStore split_config.arm64_v8a.apk

| Analysis | Link |
|----------|------|
| VirusTotal | https://www.virustotal.com/gui/file/5d1dbb8fd44703c350ab33055c9c53ce12a158beaf22cc64b65e6653f9ca2d85   |
| Apklab     | https://apklab.io/apk.html?hash=5d1dbb8fd44703c350ab33055c9c53ce12a158beaf22cc64b65e6653f9ca2d85   |

----------------

### 1.2. Changelog

These are the main changes in versions 2.0.8/2.0.9 - compared to version 2.0.7

- **Removed ALL references to iFlyTek (AIUI / MSC), except for the AI Translation service link**
- - Its business name no longer appears in the latest version (01/31) of the Privacy Policy
- - removed AIUI SDK
- - disabled the creation of `msc` folder-copy in `/storage/emulated/0`
- - removed `iflytek` & `msc` folders from `/data/user/0/com.systoon.dongaotoon/files`
- - removed all conns to logconf.iflytek.com
- - removed shared prefs
.
- **Removed ALL references to Sensors Data, except for API endpoint resolution in internal DNS Alias/Host**
- - Its business name no longer appears in the latest version of the Privacy Policy
- - removed events & device-info DB from `/data/user/0/com.systoon.dongaotoon/databases`
- - removed shared prefs
.
- **Added MobileFFmpeg (_no longer mantained - yet compatible with minSdk API 21 LP5.0_)**

APK `/lib` & `/data/user/0/com.systoon.dongaotoon/` arrangement 2.0.7 -> 2.0.8/2.0.9 .

| ![alt text](doc_img/img22.png "Removed iFlyTek / Sensors Data in /data") |
|--------------------------------------------------------------------------|
| ![alt text](doc_img/img25.png "Removed iFlyTek / Sensors Data in /libs") |
| ![alt text](doc_img/img23.png "Removed iFlyTek / Sensors Data in /libs") |
| ![alt text](doc_img/img24.png "Removed iFlyTek / Sensors Data in /libs") |

\
Besides, these are the further main changes in release 2.0.8 build #22020118 (only available from Play Store) compared to Releases 2.0.8 Build #2122061137 & 2.0.9 Build #2122096155 (Universal APK packages):

- **Removed Read and Write permissions for CONTACTS**
- **Removed Read permission for CALENDAR**
- - Still there the Write permission for CALENDAR, that allows MY 2022 to add events into user's calendar, obviously if the user grants such permission too
- **Fixed the language-switch for dialogs/texts/UI when device-language isn't either Chinese or English**
- - Foreign-language users get everything in English language - including the Privacy Policy and User Agreement from the Agreement Dialog
- **In-app description of the purposes for the storage permission request**
- - Obviously still there the exposure/tampering risk of such folders, since inside internal shared storage & without R/W restrictions
- Removed MobileFFmpeg

| Tips / Storage-Perm Dialog - 2.0.8 Build #22020118 | Normal Permissions - 2.0.8 Build #22020118 |
|----------------------------------------------------|--------------------------------------------|
| ![alt text](doc_img/img29.png "New Tips")          | ![alt text](doc_img/img30.png "New Perms") |

\
Release 2.0.9 build #22030518 (only available from Play Store) uses the same functional code of release 2.0.8 build #22020118. It just includes the removal of unused permissions = Bluetooth / NFC / Network change + some vendor-specific perms. 

----------------

## 2. Some analysis tips

MY 2022 uses SecNeo Apkwrapper for (_static & dynamic_) code obfuscation / anti-debug protection purposes.

This means that SecNeo helper halts the app execution in any of the following situations :

- **Root detection** = if SecNeo helper's root-check is triggered on app-launch, the execution gets immediately halted. MY 2022 main process won't crash, but the child processes (managed by SecNeo) will fully stop.

![alt text](doc_img/img02.png "SecNeo Root Check")

- **Emulator detection** = same behaviour of root-detection.

- **Trace/Debug detection** = it trace/debug activities get detected vs the main process, MY 2022 immediately crashes - also leading to side-effects (including device-freeze sometimes).

\
Therefore, I had to check & collect infos through multiple approaches :

1. **Verbose Logcat** 

2. **MITM** = running MY 2022 on an Android Marshmallow 6.0 (API Level 23) real-device is enough to intercept HTTPS/SSL requests/responses too - together with the other network traffic captures.

3. **Injection into libc++_shared.so** = it's successful, although it requires some (small) adjustments + (a litle bit of) patience. Used the [same logic/approach - applied by **Marcus Mengs** to the DJI Go 4 app](https://twitter.com/mame82/status/1335987735388188672).  

4. **Root switch OFF/ON** = since SecNeo helper performs the root-checks only on app-launch stage, ADB/App Root permissions can be hidden/disabled before launching the app & fully enabled again later. This allows to install Frida server, to use strace, etc. . Just keep MY 2022 app still running - even while in background. Otherwise, if you close it, you'll have to clear its cache+data storage & reset/restart from scratch. 

> **Thumb rule :** since SecNeo attaches to itself with a child process so that you can't attach directly to the main one, avoid any attach attempt to the main one ;) . 
>
> Just take advantage from the fact that the memory between the two processes is cloned - which will let you find the patched functions in the main process via the child one.

----------------------------------

## 3. Let's see now what we've just downloaded

The first thing that catches the eye is obviously the (high) number of declared permissions for both Release 2.0.8 builds.

| **Release 2.0.8 Build #2122061137** = Universal APK - Available from most Official Sources  |
|---------------------------------------------------------------------------------------------|
| ![alt text](doc_img/img03.png "MY 2022 Numbers")                                            |
| **Release 2.0.8 Build #22020118 = Bundle AAB - Available only from Google Play Store**      |
| ![alt text](doc_img/img28.png "MY 2022 Numbers")                                            |

> _Side Note :_ 
>
> Such declared permissions' number was [_even bigger_](https://reports.exodus-privacy.eu.org/en/reports/search/com.systoon.dongaotoon/) (67) in prior releases to 2.0.7. The following perms have been removed indeed from declaration in release 2.0.7 :
>
> | _Declared Until 2.0.6 -> Removed from 2.0.7_ | _Level_  | _Description_                                                   |
> |----------------------------------------------|----------|-----------------------------------------------------------------|
> | _android.permission.READ_PHONE_NUMBERS_      | _HIGH_   | _read device's phone number(s)_                                 |
> | _android.permission.REORDER_TASKS_           | _LOW_    | _change the Z-order of tasks_                                   |
> | _android.permission.SYSTEM_OVERLAY_WINDOW_   | _HIGH_   | _Deprecated API 26 - requires system privileges in Oreo/higher_ |

However, there are some factors to keep in mind because of the minSdkVersion API Level 21 (**Lollipop 5.0 - 1st Release October 2014**) :

1. **It requires some deprecated permissions, that can't be granted since Oreo 8.0 API 26 (August 2017) - and some even since Marshmallow 6.0 API 23 (August 2015) - without further special privileges** = system-apps or manually authorized user-apps. Users with devices running Oreo or higher should indeed manually grant system-write special settings (_special permissions for apps_) or set MY 2022 as device-administration app...

2. **It requires vendor-specific extra-perms, that are needed/used only by devices running Lollipop 5.0 & 5.1** = devices running Marshmallow or higher don't need/use them.

\
On Q3 2021 the platform-share of Lollipop 5.0 + 5.1 was already less than 4% (_according to both Android Studio Dashboard & 3rd party worldwide version-share stats_). Besides, the current combined platform-share of all Pre-Oreo versions is anyway in the range 10-15%. 

Therefore, 31 perms can be safely excluded from the watchlist, in order to **focus only on the remaining perms (_33 or 30, according to the installed build_) that apply to the marketed devices in the last 5 years**.  

Then, since such 30/33 perms have different risk-levels, let's group them according to perm HIGH/LOW level & to MY 2022 requests of such the permission during its execution/activities.

\
**A.** Monitoring read-only access to phone state (_including the current cellular network information & the status of any ongoing calls_) & read/write the user's contacts data.

| **PERM Control Group A**                          | Level  | Description                                               |
|---------------------------------------------------|--------|-----------------------------------------------------------|
| android.permission.DISABLE_KEYGUARD               | LOW    | disable the keyguard if it is not secure                  |
| com.android.launcher.permission.INSTALL_SHORTCUT  | LOW    | add shortcut in launcher (**NOT WORKING in Oreo/higher**) |
| android.permission.KILL_BACKGROUND_PROCESSES      | LOW    | kill background processes                                 |
| android.permission.READ_CONTACTS **(!)**          | HIGH   | read contact data                                         |
| android.permission.READ_PHONE_STATE               | HIGH   | read phone state and identity                             |
| android.permission.WRITE_CONTACTS **(!)**         | HIGH   | write contact data                                        |

> **(!) Such perms aren't declared/supported anymore in Release 2.0.8 build #22020118 - available from Google Play Store.**

\
**B.** Monitoring High Level perms - compulsory read/write storage + dynamically required by [AIUI iFlyTek](https://doc.iflyos.cn/aiui/sdk/mobile_doc/quick_start.html#%E5%BC%80%E5%8F%91%E6%AD%A5%E9%AA%A4) (AI Voice Translation) on release 2.0.7 builds (**AIUI SDK has been removed in both release 2.0.8 builds**) & QR code scanner & in-app messages

| **PERM Control Group B**                          | Level  | Description                                               |
|---------------------------------------------------|--------|-----------------------------------------------------------|
| android.permission.ACCESS_BACKGROUND_LOCATION     | HIGH   | location in background (**requires granted coarse/fine**) |
| android.permission.ACCESS_COARSE_LOCATION         | HIGH   | coarse (network-based) location                           |
| android.permission.ACCESS_FINE_LOCATION           | HIGH   | fine (GPS) location                                       |
| android.permission.CALL_PHONE                     | HIGH   | directly call phone numbers                               |
| android.permission.CAMERA                         | HIGH   | take pictures and videos                                  |
| android.permission.READ_CALENDAR **(!)**          | HIGH   | read calendar events                                      |
| android.permission.READ_EXTERNAL_STORAGE          | HIGH   | read external storage contents                            |
| android.permission.RECORD_AUDIO                   | HIGH   | record audio                                              |
| android.permission.WRITE_CALENDAR                 | HIGH   | add or modify calendar events                             |
| android.permission.WRITE_EXTERNAL_STORAGE         | HIGH   | read/modify/delete external storage contents              |

> **(!) Such perm isn't declared/supported anymore in Release 2.0.8 build #22020118 - available from Google Play Store.**

\
**C.** Monitoring Low level perms -> useful to better spot SecNeo child processes (_for instance when they check Wi-Fi status before attempting the coarse location request_)

| **PERM Group C**                                  | Level  | Description                                               |
|---------------------------------------------------|--------|-----------------------------------------------------------|
| android.permission.ACCESS_LOCATION_EXTRA_COMMANDS | LOW    | access extra location provider commands                   |
| android.permission.ACCESS_NETWORK_STATE           | LOW    | change network connectivity                               |
| android.permission.ACCESS_WIFI_STATE              | LOW    | view Wi-Fi status                                         |
| android.permission.BLUETOOTH **(!)**              | LOW    | create Bluetooth connections                              |
| android.permission.BLUETOOTH_ADMIN **(!)**        | LOW    | bluetooth administration (_discover and pair BT devices_) |
| android.permission.CHANGE_NETWORK_STATE **(!)**   | LOW    | change network connectivity                               |
| android.permission.CHANGE_WIFI_STATE              | LOW    | change Wi-Fi status                                       |
| android.permission.FOREGROUND_SERVICE             | LOW    | allow non-system app to use Service.startForeground       |
| android.permission.INTERNET                       | LOW    | full internet access                                      |
| android.permission.MODIFY_AUDIO_SETTINGS          | LOW    | change your audio settings                                |
| android.permission.NFC **(!)**                    | LOW    | control Near-Field Communication (tags, cards, readers)   |
| android.permission.RECEIVE_BOOT_COMPLETED         | LOW    | automatically start at boot                               |
| android.permission.VIBRATE                        | LOW    | control vibrator                                          |
| android.permission.WAKE_LOCK                      | LOW    | prevent phone from sleeping                               |

> **(!) Such perms aren't declared/supported anymore in Release 2.0.9 build #22030518 - available from Google Play Store.**

\
**D.** Monitoring the remaining perms

| **PERM Group D**                                  | Level  | Description                                                    |
|---------------------------------------------------|--------|----------------------------------------------------------------|
| android.permission.GET_TASKS                      | HIGH   | retrieve running apps (deprecated API21 - used for root check) |
| android.permission.RESTART_PACKAGES               | LOW    | restart bkg procs (**deprecated API15 - NO longer supported**) |
| android.permission.USE_FINGERPRINT                | LOW    | allow use of fingerprint                                       |

----------------------------------

Now it's time to (finally) run MY 2002 on some test-devices.

-------------------------

## 4. Start me up!

When you launch the app you're prompted with the splash screen & the **User Agreement and Privacy Policy** dialog. 

Here the users can eventualy run into an unsolved issue, that drags on from previous versions (_as you can also read on Play Store app reviews_).

**MY 2022 isn't a fully multilingual app. It supports only Chinese - as primary/default language - and English.**

| Welcome Dialog (Chinese & non-English)        | Welcome Dialog (English)                      |
|-----------------------------------------------|-----------------------------------------------|
| ![alt text](doc_img/img05.png "Welcome - CN") | ![alt text](doc_img/img06.png "Welcome - EN") |

**Therefore, if the device system-language is other than English or Chinese and you're not using Release 2.0.8 Build #22020118 or Release 2.0.9 Build #22030518 - available from Google Play Store, then you'll get everything in Chinese... UI strings, all text dialogs and even external pages/websites - recalled by the app**.

Such _'lost in translation'_ situation also applies to User Agreement and Privacy Policy, that can be read in-app only. So - _if you run into such issue_ - you won't even be able to open the links into your web-browser & use online-translation services.

> _**Side Note :**_
>
> **These are anyway the direct links to the English versions of such documentation.**
>
> | Document - English version | Direct Link                                            |
> |----------------------------|--------------------------------------------------------|
> | User Agreement EN - JAN 31 | https://dongaostatic.beijing2022.cn/fdfsdownload/group1/M00/01/F7/CoA4XGH3mS6AWkhPAACM8xUW_TM39.html |
> | Privacy Policy EN - JAN 31 | https://dongaostatic.beijing2022.cn/fdfsdownload/group1/M00/01/F7/CoE4fGH3mS6AJ3T9AADkOacS3VU67.html |

So users facing such issue should accept or reject the agreement without reading the related documentation, unless they click on the privacy-links in the International versions of Play Store / Huawei AppGallery / Tencent.

In such case they will still get only the Privacy Policy in Chinese language, but at least they will be able to read it - by using an online-translation service.

| MY 2022 Privacy Policy on | Direct Link                                             |
| --------------------------|---------------------------------------------------------|
| Google Play Store         | https://dongaostatic.beijing2022.cn/fdfsdownload/group1/M00/01/DD/CoA4XGHz3EKAWRrqAADBOuE9gE093.html |
| Huawei AppGallery         | https://dongaostatic.beijing2022.cn/fdfsdownload/group1/M00/01/8C/CoA4XGHr1Y2AJPCAAAC3vkCbbzs35.html |
| Samsung Galaxy Store      | _ehm..._                                                         |
| Tencent App Store         | https://cftweb.3g.qq.com/privacy/agreement?appid=54137426   |

\
Once you tap to agree, the app immediately submits the onboarding requests to its main API endpoint togethet with the device-infos to Tencent (_Bugly Analytics_) & iFlyTek (in order to retrieve the msg to perform the API enrollment).


Protocol | Type | Host                       | Path               | Params         | Body     |
|--------|------|----------------------------|--------------------|----------------|----------|
| HTTP   | POST | android.bugly.qq.com       | /rqd/async         | URL : ?aid=... | OMISSIS  |
| HTTP   | POST | t20foshanad.zhengtoon.com  | /api/1.0/getAdInfo |                | {"clientSystem":1, |
|        |      |                            |                    |                | "height":<YOUR_DISPLAY_HEIGHT>, |
|        |      |                            |                    |                | "positionCode":"appStartPage", |
|        |      |                            |                    |                | "toonType":"137", |
|        |      |                            |                    |                | "width":<YOUR_DISPLAY_WIDTH>} |
| HTTP   | POST | t100knvphoto.zhengtoon.com | /api/headline/tags | 
| _HTTPS_  | _POST_ | _logconf.iflytek.com_ **(!)** | _/hotupdate_         |                | _{"appid":"5d2386c983",_ | 
|        |      |                            |                    |                | _"ver":"-1",_ |
|        |      |                            |                    |                | _"impl":2,_ |
|        |      |                            |                    |                | _"pkg":"com.systoon.dongaotoon"}_ |

> **(!) NO more in release 2.0.8 because of iFlyTek removal**

\
Then it requires (not ask) to get the read/write storage permission in order to "use this function".

> At least Release 2.0.8 Build #22020118 and Release 2.0.9 Build #22030518 - available only from Google Play Store - provide a description of the purposes for which MY 2022 is requesting the storage-perm.

| MY 2022 Perm Request - Release 2.0.8 Build #2122061137  | MY 2022 Perm Request - Release 2.0.8 Build #22020118 |
|---------------------------------------------------------|------------------------------------------------------|
| ![alt text](doc_img/img07.png "MY2022 Storage")         | ![alt text](doc_img/img29.png "MY 2022 Storage")     |

Once you've confirmed the permission, MY 2022 creates 4 new folders into the internal storage.

- **dongao** = gif file that will be used as splash screen on next app-launch

- **dongaotoon** = Tencent log files

- _**msc** = iFlyTek AIUI keys (used for AI Voice Translation service)_ **(!)**

- **msgseal** = background location log

> **(!) NO more in release 2.0.8 because of iFlyTek removal**

| ![alt text](doc_img/img08a.png "Dongao")         | ![alt text](doc_img/img08b.png "Dongaotoon") |
|--------------------------------------------------|----------------------------------------------|
| ![alt text](doc_img/img08c.png "Msc")            | ![alt text](doc_img/img08d.png "msgseal")    |

This is the new splash screen that you will see next time you'll launch again the app.

![alt text](doc_img/splash_0.gif "Splash")

\
Meanwhile MY 2022 is checking if there are updates for its 'internal HTTP DNS' Endpoint-Aliases (_used by its functions_) - Hosts. If there are any, it retrieves the new JSON & updates the entries.

Then it starts the API enrollment of the app/clientID with endpoint/controller request/response handshake.

- **Internal HTTP DNS (JSON)** - location : `data/user/0/com.systoon.dongaotoon/files` folder

| Protocol  | Alias                                  | HOST                        | PATH / PORT                        | PARAMS              |
|-----------|----------------------------------------|-----------------------------|------------------------------------|---------------------| 
| HTTPS     | api.org-contact.systoon.com            | my2022.beijing2022.cn       | /org-contact-api                   |                     |
| HTTPS     | api.org-contacts-sync.systoon.com      | my2022.beijing2022.cn       | /org-contacts-sync                 |                     |
| HTTPS     | api.receive.sensorsdata.com            | bigdata.beijing2022.cn      | /sa?project=production             |                     |
| HTTPS     | api.report.systoon.com                 | my2022.beijing2022.cn       | /content-report                    |                     |
| HTTPS     | api.workbench.torg.systoon.com         | my2022.beijing2022.cn       | /org-workbench-api                 |                     |
| HTTPS     | admanage.systoon.com                   | my2022.beijing2022.cn       | /admanage                          |                     |
| HTTPS     | api.medal-table.systoon.com            | dongaoserver.beijing2022.cn | /medal-table                       |                     |
| HTTPS     | api.certservice.systoon.com            | my2022.beijing2022.cn       | /uias                              |                     |
| HTTPS     | api.homepage.systoon.com               | my2022.beijing2022.cn       | /homepage-api                      |                     |
| HTTPS     | api.torg.systoon.com                   | my2022.beijing2022.cn       | /org-tproxy                        |                     |
| DATASTORE | api.contact.systoon.com                | my2022.beijing2022.cn       | :8099                              |                     |
| HTTPS     | api.certification.systoon.com          | my2022.beijing2022.cn       | /uias                              |                     |
| HTTPS     | api.homepage.fzinteraction.systoon.com | my2022.beijing2022.cn       | /cytflinteraction-interaction-app/ |                     |
| HTTPS     | datoon.event.arrangement.en            | my2022.beijing2022.cn       | /uias/auth/authorize               | clientId = OMISSIS  |
|           |                                        |                             |                                    | display  = h5       |
|           |                                        |                             |                                    | redirectUri=https%3A%2F%2Fdongaostatic.beijing2022.cn%2Fdatoon-medal-list-h5%2Fm%2F%23%2FmedalList  |
|           |                                        |                             |                                    | responseType = code |
|           |                                        |                             |                                    | scope = userInfo    |
|           |                                        |                             |                                    | state = en          |
|           |                                        |                             |                                    | sign = OMISSIS      |
| HTTPS     | api.tsearch.systoon.com                | my2022.beijing2022.cn       | /search                            |                     |
| HTTPS     | datoon.event.arrangement.zh            | my2022.beijing2022.cn       | /uias/auth/authorize               | clientId = OMISSIS  |
|           |                                        |                             |                                    | display  = h5       |
|           |                                        |                             |                                    | redirectUri=https%3A%2F%2Fdongaostatic.beijing2022.cn%2Fdatoon-medal-list-h5%2Fm%2F%23%2FmedalList  |
|           |                                        |                             |                                    | responseType = code |
|           |                                        |                             |                                    | scope = userInfo    |
|           |                                        |                             |                                    | state = zh          |
|           |                                        |                             |                                    | sign = OMISSIS      |
| HTTPS     | api.account.systoon.com                | my2022.beijing2022.cn       | /toonuser                          |                     |
| HTTPS     | api.operating.systoon.com              | my2022.beijing2022.cn       | /content-operating                 |                     |
| HTTPS     | api.audit.systoon.com                  | my2022.beijing2022.cn       | /content-audit                     |                     |
| HTTPS     | api.ocmconfig.systoon.com              | my2022.beijing2022.cn       | /app-config-api                    |                     |
| HTTPS     | api.media-bank.systoon.com             | tmail.beijing2022.cn        | /mediabank/                        |                     |
| HTTPS     | otm.temail.manage                      | tmail.beijing2022.cn        | /otm/public                        |                     |
| HTTPS     | api.temail-image-storage.systoon.com   | tmail.beijing2022.cn        | /avatartmail/                      |                     |
| DATASTORE | api.temail-gateway.systoon.com         | tmail.beijing2022.cn        | :8099                              |                     |
| HTTPS     | api.push-gw.systoon.com                | tmail.beijing2022.cn        | /pushgw/                           |                     |
| HTTPS     | api.temail-oss.systoon.com             | tmail.beijing2022.cn        | /otm/public                        |                     |
| HTTPS     | api.temail-domain.systoon.com          | tmail.beijing2022.cn        | /                                  |                     |
| HTTPS     | api.temail-auth.systoon.com            | tmail.beijing2022.cn        |                                    |                     |

- **Hardcoded Endpoints**

| Protocol | Host                     | Path        | Params                    |
|----------|--------------------------|-------------|---------------------------|
| HTTP     | contentt600.systoon.com  | /groupapi   |                           |
| HTTP     | contentt600.systoon.com  | /support    |                           |
| HTTP     | contentt600.systoon.com  | /headline   |                           |
| HTTP     | cardt600.systoon.com     | /dfs        |                           |
| HTTP     | cardt600.systoon.com     | /card       |                           |
| HTTP     | cardt600.systoon.com     | /tcard      |                           |
| HTTP     | developt600.systoon.com  | /applib     |                           |
| HTTP     | noticeappt600.systoon.com|             |                           |
| HTTP     | tmailc.huairoutoon.com   |             |                           |
| HTTPS    | da.systoon.com           | /config/    | URL : ?project=production |
| HTTPS    | da.systoon.com           | /config/    | URL : ?project=default    |
| HTTPS    | da.systoon.com           | /sa/        | URL : ?project=production |
| HTTPS    | da.systoon.com           | /sa/        | URL : ?project=default    |

\
API enrollment takes about 5-10 seconds (_according to the network traffic performances_). Meanwhile, onscreen there is only a white page without any fancy animation = no spinning wheel or progress bar, just the "Loading..." text together with the Beijing Games 2022 logo. 

Once finished you (finally) get the home of the app with ACR instructions dialog.

| MY 2022 Home                                    | ACR Accreditation Card Dialog                   |
|-------------------------------------------------|-------------------------------------------------|
| ![alt text](doc_img/img09.png "MY2022 Home")    | ![alt text](doc_img/img10.png "ACR Dialog")     |

\
However, after that you've dismissed the instructions dialog, you'll soon realize that there are very few services you can use without authentication = only 2.

- **Headline News** from Beijing 2022 Games Press Centre 

- **Beijing 2022 Encyclopedia** = thematic articles related to the Olympic Games

| MY 2022 News                                    | MY 2022 Services                                |
|-------------------------------------------------|-------------------------------------------------|
| ![alt text](doc_img/img11.png "MY2022 News")    | ![alt text](doc_img/img12.png "MY2022 Services")|

\
Therefore, if you would like to use any other service or feature of MY 2022, even including the QR code scanner and the weather portal without map support, you should login with the ACR credentials or SMS OTP code (_only sent to Chinese numbers of already accredited users_).

| ACR Login                                       | SMS Login (_Accredited Chinese numbers only_)   |
|-------------------------------------------------|-------------------------------------------------|
| ![alt text](doc_img/img13.png "ACR Login")    | ![alt text](doc_img/img14.png "SMS Login")        |

Or not ;) ?

\
MY 2022 works indeed like a web-browser with additional plugins/extensions & most services are just external websites.

So it's quite easy to find their URLs, especially if you have re-enabled the root perms in your device (_[chapter 2. Some analysis tips](#2-some-analysis-tips)_).

You can already spot some URLs of the app services - just by checking the verbose logs (_you can also cross-check your logs with the raw ones I've uploaded in this repo_) & MITM intercepted request/response handshakes.

```
	"appIcon":"https://dongaostatic.beijing2022.cn/fdfsdownload/group1/M00/00/BB/CoA4XGHK9fOALa5gAAAOeZubRtQ108.png",
	"appId":2729,
	"appParamRelationOutput":{},
	"appPic":"https://my2022.beijing2022.cn/fdfsdownload/group1/M00/00/25/wHw7fWEnH5SACaeNAAANB-h5jIk848.png",
	"appSubTitle":"",
	"appTitle":"Customs Declaration",
	"appType":0,
	"appUrl":"https://health.customsapp.com/healthweb/home/pages/healthDeclare/declare.html",
```

Besides, once you re-enable/unhide ADB/app root perms _(just keep MY 2022 app running_), you can also access to the `/data/user/0/com.systoon.dongaotoon` folder and monitor/dump its full content.

```
.cache
.payload
app_bugly
app_crashrecord
app_database
app_textures
app_webview
cache
code_cache
databases
files
log
shared_prefs
.edata
```

**This obviously allows you to retrieve everything... not just all the URLs of MY 2022 app-services from the databases/files ;)** . 

Besides, dumping is even more trivial in release 2.0.7 builds since you can also rely on the `com.systoon.dongaotoon` DB - used by Sensors Data SDK to track ALL MY 2022 app activities/events (both user-initiated and app-initiated ones). Such DB is no more available in 2.0.8 builds - owing to Sensors Data SDK removal - but you can obviously still retrieve data/infos from the other dumped databases / files. 

\
Then, except for AI Translation, which requires iFlyTek's AIUI SDK, the other services can be used from web browser on any device without MY 2022.

Besides, some services - such as VR Virtual Arena and Weather Portal - are even more valuable if you use them from a web browser on a computer or tablet.

| MY2022 App Services     | Link                                                                                         |
|-------------------------|----------------------------------------------------------------------------------------------|
| News                    | https://www.beijing2022.cn/wap_en/headlines.htm                                              |
| Content Report          | https://dongaostatic.beijing2022.cn/notice-reporting/index.html?language=en                  |
| Health Monitor System   | https://hms.beijing2022.cn/                                                                  |
| Green Health Code       | https://hrhk.cs.mfa.gov.cn/h5/                                                               |
| Customs Declaration     | https://health.customsapp.com/healthweb/home/pages/healthDeclare/declare.html                |
| Baggage Tracking        | https://xl.bcia.com.cn/app_static/standard2.0/en/index.html#/LuggageSD                       |
| Weather Portal          | https://olympic.weather.com.cn/m/en_dongao                                                   |
| AI Translation          | https://huiyi.iflyrec.com/iflyrec-dat/smartTranslation.html#                                 |
| Gaode Map/Navi          | https://m.amap.com                                                               |
| Official Store          | https://dongaostatic.beijing2022.cn/datoon-official-mall/m/#/home?language=en                |
| Smart Arena             | https://dongaostatic.beijing2022.cn/datoon-service-authorisation/m/#/smartVenues?language=en |
| Encyclopedia (Sports)   | https://dongaostatic.beijing2022.cn/datoon-encyclopedia-h5/m/#/main?language=en&active=1     |
| Encyclopedia (Mascots)  | https://dongaostatic.beijing2022.cn/datoon-encyclopedia-h5/m/#/main?language=en&active=2     |
| Encyclopedia (Emblem)   | https://dongaostatic.beijing2022.cn/datoon-encyclopedia-h5/m/#/main?language=en&active=3     |
| Encyclopedia (History)  | https://dongaostatic.beijing2022.cn/datoon-encyclopedia-h5/m/#/main?language=en&active=4     |
| Encyclopedia (Partners) | https://dongaostatic.beijing2022.cn/datoon-encyclopedia-h5/m/#/main?language=en&active=5     |

----------------------------------

## 5. Privacy Matters & Data Security

### 5.1. Permissions

It's undeniable that 64 declared perms are many, as well as 33 = perms that can be really used by Oreo 8.0+ devices w/o manually forcing special perms (_[chapter 3. Let's see now what we've just downloaded](#3-lets-see-now-what-weve-just-downloaded)_).

Yet, my audits show NO evidence of perms abuse by MY 2022 - either in unauthenticated mode (granted r/w storage only) or in authenticated mode (granting of further normal perms).

| Unauthenticated mode                            | Authenticated & granted 33 normal perms         |
|-------------------------------------------------|-------------------------------------------------|
| ![alt text](doc_img/img15.png "NO-AUTH")        | ![alt text](doc_img/img16.png "AUTH+NormalPERM")|

\
Even though - for instance - the background location worker of MY 2022 is active since 1st app-launch, it can't record the location into its log file (`msgseal` folder in internal storage) until the user grants the location permission - if/when requested by MY 2022 services.

| Granted Location Permission                       | Background Location Log | Location Logs Folder |
|---------------------------------------------------|-------------------------|----------------------|
| ![alt text](doc_img/img17.png "Granted Loc Perm") | ![alt text](doc_img/img18.png "Bkg Loc Log") | ![alt text](doc_img/img19.png "Loc Logs Folder") |

Such periodic location recording in background starts indeed only when the user grants the location permission.

> **Exposure and/or Tampering Risks**
>
> - **!! Here the real issue is the logs' location itself !!**
>
> - Collection of cleartext rw logfiles inside `msgseal` folder, that can be accessed both by users and other apps with granted storage perms.
>
> - The same risks apply to iFlyTek's AIUI keys' `msc` folder (**removed in release 2.0.8**) & even to (_non plaintext_) Tencent logs' folder (`dongaotoon`) too.

\
I've also performed further audits in Authenticated mode + Special Perms = allowed system-write + app-overlay perms in order to spot any perm-abuse (_including any interference with other apps' execution_) by MY 2022 - not detected by granting normal perms only.

| Authenticated & granted 33 normal perms          | Special Perms = system-write + app-overlay      |
|--------------------------------------------------|-------------------------------------------------|
| ![alt text](doc_img/img20.png "AUTH+NormalPERM") |![alt text](doc_img/img21.png "SpecialPERM")     |

Even in such special case I haven't found any anomaly. 

> _**Side notice :**_
>
> _Attached to these notes you also find some raw execution [logs](logs) - retrieved from non-rooted Huawei devices (Mate 40 - P40 Pro - Nova 5T) while running MY 2022 rel. 2.0.7 & 2.0.8 & 2.0.9 in these 3 different scenarios :_
>
> - _Unauthenticated mode with granted storage perm only_
> - _Authenticated mode with granted 33 normal perms_
> - _Authenticated mode with granted 33 normal perms + special perms_
>
> _I uploaded them in full, without any changes, so you will also see references to other events, unrelated to MY 2022 processes, as well as details about the device & the network connection._

\
Just 2 notes about the microphone perm matter :

- While monitoring IFlyAIUIEvent I haven't matched any autonomous listening/recording behaviour after MY 2022 installation on the test-devices, even granting full normal+special perms.

- Now the question is closed, just owing to the release of MY 2022 version 2.0.8 for Android = full removal of iFlyTek / AIUI SDK code together with the deletion of the company name from MY 2022 privacy policy.

---------------

### 5.2. Device Info & Tracking

There are no data exfiltration activities by MY 2022 processes.

Even the collected data about the hardware specs & software configuration of the device aren't so many as those you can see for instance through CPU-Z or Device Info HW.

However, there must be a valid reason if Sensors Data has been ousted (together with iFlyTek) from all MY 2022 projects & removed from third-parties' list too.

| ![alt text](doc_img/img26.png "Removed Sensors Data in /data") |
|----------------------------------------------------------------|
| ![alt text](doc_img/img27.png "Removed Sensors Data in /libs") |

Anyway the level of in-app events' tracking is (predictably) high, also including the content-audit related to the messaging platform.

However, I just didn't have enough time in order to check it.

I've just managed to take a quick look at the 3rd-party websites of the MY 2022 services & find that there are less additional trackers than expected.

| MY2022 App Services     | Trackers  | Ads |
|-------------------------|-----------|-----|
| News                    | 3         | -   |
| Content Report          | -         | -   |
| Green Health Code       | 3         | -   |
| Customs Declaration     | -         | -   |
| Baggage Tracking        | -         | yes |
| Weather Portal          | 1         | -   |
| AI Translation          | -         | -   |
| Official Store          | -         | -   |
| Smart Arena             | 1 (*)     | -   |
| Encyclopedia            | -         | -   |

> (*) found on bj2022.rushivr.com URLs - VR hosts

------------------------------

## 6. Conclusions

In summary, there are some issues still there - even with 2 official releases (2.0.7 & 2.0.8) in just 3 days.

> **Exposure and/or Tampering Risks for**
>
> - Background location logs (cleartext rw files) inside `msgseal` folder, that can be accessed w/o restrictions by users and other apps with granted storage perms.
>
> - iFlyTek's AIUI keys copied into `msc` folder (**removed in release 2.0.8 - but still there in 2.0.7**)
>
> - Tencent logs' folder `dongaotoon` - related to Mars-Xlog

> **Vulnerabilities in data transmission** _(unchanged since [Citizen Lab report](https://citizenlab.ca/2022/01/cross-country-exposure-analysis-my2022-olympics-app/))_
>
> - Vulnerable to trivial MITM
>
> - Some traffic still unencrypted = bad SSL and even HTTP-only

> **Minor bugs, not fixed yet after months**
>
> - Wrong language automatic switch for app onboarding (_not fixed yet in release 2.0.8 packages available from other official sources rather than Google Play Store_)
>
> - Captcha & ACR login form issues with some well-known 3rd-party keyboards such as SwitfKey

Anyway nothing astonishing at all. Choosing targetSdk API Level 28 (Pie 9.0) with minSdk API 21 (Lollipop 5.0) leads to such situations with unneeded extra-perms & deprecated/critical code.

Oreo 8.0 (26) or at worst Marshmallow 6.0 (23) would have been for sure a better minSdk choice for code compatibility, but now it's too late since the Olympic Games are starting in 2 days. 

My 2 cents & obv Good Luck to all the athletes ;)

----------------

## 7. Appendix

As I've written in the introduction, since most 3rd-party reports were mainly focused anyway on iOS versions of MY 2022 app, I've taken a (quick) look at its releases for Android.

So I've only checked the current releases - 2.0.7 and 2.0.8 - available for Android from the official stores/sources. I haven't looked at previous releases because my purpose wasn't anyway to review the history of MY 2022 app code. Besides, I wanted to allow other users to crosscheck my findings with the available release builds from official sources... not with unchecked APKs available from third-party repos.

\
In the end, I've found no evidence of any malicious activities, such as data exfiltration or covert audio recording/streaming, while checking MY 2022 releases 2.0.7 & 2.0.8 & 2.0.9 for Android.

**According to such releases' analysis, MY 2022 is just a poorly developed app with its obvious bugs & security issues (_i.e archived logs/appdata into the internal shared storage_). 'Ehm' development that is heavily related to some questionable project choices, such as keeping minSDK API Level 21 (Lollipop 5.0 - 1st Release October 2014)**.

End of the story... or so I've thought (_and hoped too_).

\
Some users started indeed asking me about some features, that weren't available at all in release 2.0.7 & 2.0.8 builds...

Therefore I've taken another (very quick) peek, this time into MY 2022 app's past = I've picked a 3+ months ago release build as the sample for this further analysis.

| Source       | Release | Build       | Link                                                                 |
|--------------|---------|-------------|----------------------------------------------------------------------|
| QQ / Tencent | 2.0.1   | #2110270441 | http://imtt.dd.qq.com/16891/apk/4106F46DD3F7F56F40F5A20CAD60B84F.apk |

\
These are some online-analysis references of such sample.

- **Universal APK 2.0.1 #2110270441** - w/o duplicated armeabi-v7a libs (_bc upload limits_)

| Analysis | Link |
|----------|------|
| Pithus (ARM-only)     | https://beta.pithus.org/report/0a9196612c761d7009c730924070137f6ad69ece287b2f8270b62d46d46b1e8c   |
| JoeSandbox (ARM-only) | https://www.joesandbox.com/analysis/566751/0/html   |

- **Universal APK 2.0.1 #2110270441** - full package

| Analysis | Link |
|----------|------|
| VirusTotal | https://www.virustotal.com/gui/file/a8caba9e40180cd2b2d4be5f36edaeaeaadf64c21937fe648fc5e0f050f7686a   |
| Apklab     | https://apklab.io/apk.html?hash=a8caba9e40180cd2b2d4be5f36edaeaeaadf64c21937fe648fc5e0f050f7686a   |

\
Let's check the package & diff/compare it with release 2.0.7/2.0.8.

| **Release 2.0.1 Build #2110270441** - Universal APK    |
|--------------------------------------------------------|
| ![alt text](doc_img/img31.png "MY 2022 2.0.1 Numbers") |

67 perms = the same number as for 2.0.6 builds (_check Side Note in [Chapter 3](#3-lets-see-now-what-weve-just-downloaded)_) & it means that - also in 2.0.2 / 2.0.3 / 2.0.4 / 2.0.5 builds - there were the manifest declarations of these extra-perms.

> | _Declared Until 2.0.6 -> Removed from 2.0.7_ | _Level_  | _Description_                                                   |
> |----------------------------------------------|----------|-----------------------------------------------------------------|
> | _android.permission.READ_PHONE_NUMBERS_      | _HIGH_   | _read device's phone number(s)_                                 |
> | _android.permission.REORDER_TASKS_           | _LOW_    | _change the Z-order of tasks_                                   |
> | _android.permission.SYSTEM_OVERLAY_WINDOW_   | _HIGH_   | _Deprecated API 26 - requires system privileges in Oreo/higher_ |

- Additional Activities in 2.0.1 - not found in 2.0.7 / 2.0.8

```
com.msgseal.chat.common.chatRelate.ChatReportActivity
com.msgseal.chat.utils.video.PhotoRecorderActivity
```

- Additional Services in 2.0.1 - not found in 2.0.7 / 2.0.8

```
com.systoon.customhomepage.commonlib.stepcounter.TodayStepService 
com.systoon.customhomepage.commonlib.stepcounter.JobSchedulerService
com.sitech.migurun.service.AudioPlayService (!)

com.lianjia.common.vr.server.WebViewInProcessService 
com.lianjia.common.vr.server.WebViewServer 
com.lianjia.common.vr.floatview.DebugService 

com.systoon.customhomepage.commonlib.stepcounter.receive.TodayStepBootCompleteReceiver 
com.systoon.customhomepage.commonlib.stepcounter.receive.TodayStepShutdownReceiver 
com.systoon.customhomepage.commonlib.stepcounter.receive.TodayStepAlertReceive
```

- Additional Providers in 2.0.1 - not found in 2.0.7 / 2.0.8

```
com.sensorsdata.analytics.android.sdk.SensorsDataContentProvider (!)
```

> **(!)** Still available in 2.0.7 builds

- Additional Hardcoded Endpoints in 2.0.1 - not found in 2.0.7 / 2.0.8

| Protocol | Host                           | Path                            | Params                              |
|----------|--------------------------------|---------------------------------|-------------------------------------|
| HTTPS    | t100cdconv.zhengtoon.com       | /app                            | URL : /index.html#/*                |
| HTTPS    | t100quickgoplat.zhengtoon.com  | /ningde-comment-service-api/app | URL : /m/index.html#/*              |
| HTTPS    | t200ecard.qitoon.com           |                                 |                                     |
| HTTPS    | t100ycintegral.zhengtoon.com   |                                 |                                     |
| HTTPS    | tourist.huairoutoon.com        |                                 | URL : /m/index.html#/               |

\
Even though such release 2.0.1 is an Universal package, it targets only armeabi and armeabi-v7a architectures... there are no bundled libraries for ARCH arm64-v8a.

- Additional Libs ARMv7 in 2.0.1 vs 2.0.7 / 2.0.8

```
libagora-core.so
libagora-fdkaac.so
libagora-ffmpeg.so
libagora-mpg123.so
libagora-rtc-sdk.so
libagora-soundtouch.so
libagora_ai_denoise_extension.so
libagora_dav1d_extension.so
libagora_fd_extension.so
libagora_jnd_extension.so
libagora_segmentation_extension.so

libaiui.so (!)

libAMapSDK_NAVI_v7_4_0.so (replaced by v7.8.0 in 2.0.7/2.0.8)

libARM_ARCH.so (!)

libbangcle_crypto_tool.so

libgnustl_shared.so

libMailCore.so

libmsc.so (!)
```

> **(!)** Still available in 2.0.7 builds
>
> _As you can see, release 2.0.1 included also **[Agora RTC SDK](https://github.com/AgoraIO)** dependencies_

---------------------

> ### _A short interlude..._
>
> _As some of you may have already guessed, by reading the infos in the previous chapters, **MY 2022 isn't an original app at all**._
>
> _**It's just a TOON sibling-app (packages com.systoon.<city/district>toon)**= same (deprecated) code that has been recycled. Besides the addition/removal of third-party SDK/plugins, all such TOON apps share indeed the same UI and also the same main functions under the hood._
>
> _For those who are interested, these are the references of two other TOON sibling-apps (you can check the uploaded samples in case of any download issues)._
>
> | Name / Package                 | Release | Build       | Link                                         |
> |--------------------------------|---------|-------------|----------------------------------------------|
> | com.systoon.beijinghuairoutoon | 2.0.0   | #2111041752 | https://app.systoon.com/hrt/                 |
> |                                |         |             | http://app.systoon.com/hrt/hrt.apk  (direct) |
>
> - [VirusTotal Analysis](https://www.virustotal.com/gui/file/0f814e665acf8c55ac091360778503a12e4669eba363dce9cf0fe940a24705c2)
>
> - [Pithus Report](https://beta.pithus.org/report/0f814e665acf8c55ac091360778503a12e4669eba363dce9cf0fe940a24705c2)
>
> - [JoeSandbox Report](https://www.joesandbox.com/analysis/566876/0/html)
>
> | Name / Package                 | Release | Build       | Link                                         |
> |--------------------------------|---------|-------------|----------------------------------------------|
> | com.systoon.beijingtoon        | 3.8.2   | #2122061125 | https://a.app.qq.com/o/simple.jsp?pkgname=com.systoon.beijingtoon                |
> |                                |         |             | [Direct Link from QQ server](http://imtt.dd.qq.com/sjy.20002/16891/apk/78EA8F3197F7EFDCBBF3F8578A845FAD.apk?fsname=com.systoon.beijingtoon_3.8.2_2122061125.apk&hsr=4d5s) |
>
> - [VirusTotal Analysis](https://www.virustotal.com/gui/file/18e59851846f13a11d88b76418ef8964ae2f3be0e982f1a1d07d4fd4ab5da986)
>
> - [Pithus Report - ARMv7 only (because of upload limits)](https://beta.pithus.org/report/ec67d5deca747bfe688ad36614ecfb2b1e2d1cb682c23340ff07f6e0d40d4795)
>
> - [JoeSandbox Report  - ARMv7 only (_because of upload limits_)](https://www.joesandbox.com/analysis/566884/0/html)

--------------------

Now let's install & start such MY 2022 release 2.0.1 build in order to diff/compare what it does on the device vs 2.0.7/2.0.8 builds.

You're prompted with the User Agreement / Privacy Policy dialog. Obviously, since 2.0.1 is an old release build of MY 2022, there is still the language-switch bug (_[see Chapter 4](#4-start-me-up)_) that will show everything in Chinese when device-language isn't either English or Chinese.

\
There are no differences in app-behaviour compared to 2.0.7/2.0.8 builds - just until you grant the R/W storage-permission.

Once you've granted it, you're prompted indeed with an Update alert dialog, that suggests you to update to 2.0.7 build - since the API onboarding stage detected that you're using an outdated version. 

![alt text](doc_img/img32.png "Update Alert Dialog")

If you agree, MY 2022 2.0.1 will download the 2.0.7 Universal APK from Beijing 2022 official server and even update base APK + libs (_if granted to do so_).

Anyway, you can dismiss such update-alert & keep on using the 2.0.1 build. 

\
There are also some difference about the folders, that such 2.0.1 build of MY 2022 creates & uses inside the shared internal storage.

```
dongao
dongaotoon
msc
msgseal
rsVrSdk
```

You'll immediately notice an additional folder `rsVrSdk` , that is used by **[Realsee SDK](http://realsee.com/website/openplatform/document?md=6-3)** for its **Rushi VR** venuee/indoor services ([check Smart Arena direct link in Chapter 4](#4-start-me-up)).

Besides, if you check the logs inside the `msgseal` folder, you'll notice that MY 2022 2.0.1 background periodic worker hasn't been set to record just the location (_if such perm has been granted_), but also some events related to the usage of the internal chat - owing to those additional activities of 2.0.1 (_not found in 2.0.7 / 2.0.8 builds_). 

![alt text](doc_img/img33B.png "Chat Events background worker logs")

\
So, let's re-enable/unhide ADB/app root perms on the device (_while MY 2022 2.0.1 is still running_) and check the `/data/user/0/com.systoon.dongaotooon` folder.

There are no differences in folder structure compared to 2.0.7 / 2.0.8 builds.

```
.cache
.payload
app_bugly
app_crashrecord
app_database
app_textures
app_webview
cache
code_cache
databases
files
log
shared_prefs
.edata
```

The differences are indeed inside the directories = mainly these additional databases & folders/files in 2.0.1 - not found in build 2.0.7 / 2.0.8.

```
 [+] databases
  |
[...]
  |
  |__com.systoon.dongaotoon (!)
  |__com.systoon.dongaotoon-journal (!)
  |__rs_vr.db
  |__rs_vr.db-journal

------------

 [+] files
  |
[...]
  |
  |__msc (!)

------------

 [+] shared_prefs
  |
[...]
  |
  |__com.iflytek.id.xml (!)
  |__com.iflyek.msc.xml (!)
  |__com.sensorsdata.analytics.android.sdk.SensorsDataAPI.xml (!)
  |__iflytek_collect_state.xml (!)
  |__rsvrsdk.xml
  |__sensorsdata.xml (!)
  |__vr_share_data.xml
```

> **(!)** Still available in 2.0.7 builds

\
So, after further (quick) execution checks on 2.0.1 (aka 3+ months old build of MY 2022), I've noticed in the end that :

- **There are no UI differences between 2.0.1 & 2.0.7/2.0.8/2.0.9 builds**

- - UI layout/style of 2.0.1 are the same of 2.0.7/2.0.8/2.0.9 builds (_as of the other TOON sibling-apps_), since MY 2022 2.0.1 retrieves the updated icons/images & string labels/messages from the same `*.beijing2022.cn` endpoints/paths - contacted by 2.0.7 & 2.0.8.
-
- **The level of activity tracking/logging was undeniably higher in 2.0.1 than the (_already high_) one of 2.0.7/2.0.8/2.0.9 builds, but there aren't anyway clear evidences of covert data exfiltration and/or covert audio recording/streaming activities - even in such former release (3+ months ago)**

- - MY 2022 2.0.1 performed for sure a stricter monitoring on chat activities (_as 2.0.1 users could also notice from the `msgseal` logs inside the shared internal storage_), that even involved in-app chat-exchanged media (photos/videos) for content-audit.

- - MY 2022 2.0.1 Background workers/activities submit more data to the endpoints too, but data/files upload involves only the created folders by MY 2022 inside the shared internal storage.

- - No evidence of covert file/data copy activities from user's other shared folders to such created folders

- - All spotted audio recording/streaming activities of MY 2022 2.0.1 have been anyway user-initiated. Neither spotted covert app-initiated audio-recording events nor detected suspicious uploads/streamings while MY 2022 2.0.1 closed / running in background.
-
- **The illegal-keywords on-device check (_referenced by [Citizen Lab's report](https://citizenlab.ca/2022/01/cross-country-exposure-analysis-my2022-olympics-app/)_) isn't working anymore in 2.0.1 (_although it's a 3+ months ago release = available for download way before Citizen Lab's report_)**

- - Such local check isn't working now in 2.0.1 anymore for the same reason it's not even working in 2.0.7/2.0.8 builds = no more responsive endpoints to the keyword-related requests.

- - The JSON internal DNS Aliases/Hosts (_downloaded/updated file from main API endpoint_) doesn't include anymore the resolution entries for the keyword-related endpoints. Even the search-suggestion isn't working indeed since MY 2022 attempts the connection to `defultt100keywordsearch.zhengtoon.com`... which fails because there is no either external or internal DNS resolution for such hostname.

\
That's it.

If you want to perform any further analysis / crosschecks, I've also uploaded the related sample / reports / raw logs of the MY 2022 2.0.1 build.

You'll find the MY 2022 2.0.1 stuff inside the [Appendix](appendix) folder of this repo - together with the samples + reports of the 2 referenced TOON sibling-apps.

> _**Side note :**_
>
> _I've preferred to keep MY 2022 2.0.1 stuff separated from the [samples](samples) / [raw logs](logs) / [reports](reports) folders of **MY 2022 2.0.7 / 2.0.8 / 2.0.9 builds**._ 
>
> _The current repo-folders structure should be indeed more useful to anyone who would like to carry out further analysis / cross-checks on current and/or past releases of MY 2022 app._

\
In the end, this appendix doesn't change obviously any former [conclusions](#6-conclusions) about the current MY 2022 2.0.7/2.0.8 releases for Android.

**Regardless of the (current or past) MY 2022 build you're checking, MY 2022 isn't a sophisticated spyware, but just a poorly developed app with its obvious bugs & security issues (_i.e archived logs/appdata into the internal shared storage_).**

**Poor development owing to the recycling of deprecated code (_just check the other TOON sibling-apps_) & to the resulting persistence of questionable project settings, such as keeping minSDK API Level 21 (Lollipop 5.0 - 1st Release October 2014)**.


----------------

## 8. Appendix II

As I've written since the beginning of these quick-notes, first I've focused on recent/current releases of MY 2022 = 2.0.7 & 2.0.8 for Android - in order to allow everyone to cross-check/review my analysis.

Then I've checked the old release 2.0.1 too - in order to compare its code & runtime execution with the ones of the recent/current builds. So I've picked an archived official package - still available for download from Tencent/QQ Mobile store.

I haven't randomly chosen such specific old version for the comparison. I've picked 2.0.1 indeed because :

1. It's prior to MY 2022 release 2.0.2, which was the first one with an available build for global download from the Google Play Store too

2. It fixes most bugs of former MY 2022 release 2.0.0, which added the extended support for voice-to-text functions 

\
In the end, as you have read in the former chapters + appendix, **I haven't found any runtime evidence of data exfiltration or covert audio recording/streaming activities either in MY 2022 current releases 2.0.8 or in former 2.0.7 (_where iFlyTek AIUI & SensorsData SDK were still there_)... or even in the old 2.0.1 release (_that even included the Agora RTC SDK_)**.

Since some users may be anyway interested into the analysis of other past 2.0.x releases too, I'm providing further useful infos/hints in this appendix.

\
I've collected the [samples of those past releases' official packages](appendix) & You can still download them from the official sources. So - if you want to perform hash-checks - there is no need to rely on shady 3rd-party repos to find & retrieve past MY 2022 2.0.x packages. 

- Archived MY 2022 2.0.x releases = official Universal APK builds still available for download from Tencent/QQ servers

| Release Date | Release | Build       | Direct Download from Tencent Store - archived official packages      |
|--------------|---------|-------------|----------------------------------------------------------------------|
| OCT 27, 2021 | 2.0.0   | #2110141448 | http://imtt.dd.qq.com/16891/apk/AA1C83F6FCFEDC1F18872F260989F8ED.apk |
| NOV 02, 2021 | 2.0.1   | #2110270441 | http://imtt.dd.qq.com/16891/apk/4106F46DD3F7F56F40F5A20CAD60B84F.apk |
| DEC 01, 2021 | 2.0.2   | #2111271430 | http://imtt.dd.qq.com/16891/apk/818FF0882EB04B9C9EFCA2B04F5C01E2.apk |
| DEC 28, 2021 | 2.0.3   | #2112231914 | http://imtt.dd.qq.com/16891/apk/7CC9A0D4CD0894BC08064C8118ED4857.apk |
| JAN 05, 2022 | 2.0.4   | #2112281840 | http://imtt.dd.qq.com/16891/apk/519E4B21BFE65B6FA6755DD639E87165.apk |
| JAN 25, 2022 | 2.0.6   | #2122046184 | http://imtt.dd.qq.com/16891/apk/57E0252FF65B79177B84BE17FC0B060D.apk |
| JAN 28, 2022 | 2.0.7   | #2122054203 | http://imtt.dd.qq.com/16891/apk/8336BE37378310ED1021FD8B5D39F605.apk |
| FEB 01, 2022 | 2.0.8   | #2122061137 | http://imtt.dd.qq.com/16891/apk/F6394F9915CFB58C94AD2A8D79D63148.apk |

\
Since - as usual - I've uploaded the [collected samples of MY 2022 past 2.0.x versions](appendix) to online scanner engines too, these are some online-analysis references.

- VirusTotal Analysis

| Release | Build       | VirusTotal Report                                                                                    |
|---------|-------------|------------------------------------------------------------------------------------------------------|
| 2.0.0   | #2110141448 | https://www.virustotal.com/gui/file/92d2a29449b779fae9873aec0cff1d84d98a1919f9af1fc9cd841d7371681778 |
| 2.0.1   | #2110270441 | https://www.virustotal.com/gui/file/a8caba9e40180cd2b2d4be5f36edaeaeaadf64c21937fe648fc5e0f050f7686a |
| 2.0.2   | #2111271430 | https://www.virustotal.com/gui/file/2204ccaf37dc700ab071c8a3f2d1209cc3bca9b50b95793cb9189a0e7508453e |
| 2.0.3   | #2112231914 | https://www.virustotal.com/gui/file/684fbb9ab0bcd388f55d2d474d0d795a0ae1ecafd616028fe2b1ac8a658039ff |
| 2.0.4   | #2112281840 | https://www.virustotal.com/gui/file/7284aa5e6346d3061a86fa34e63c7b2d211c3deedc5d10f985ba702cde68fac1 |
| 2.0.6   | #2122046184 | https://www.virustotal.com/gui/file/8d4ecd242769bda2e0f828994fa8db5bd569b1f31c0cfd27e2348633b06f9845 |
| 2.0.7   | #2122054203 | https://www.virustotal.com/gui/file/9d421d883c695c41bfff3cc7ae35d5de047e3c83057ce607b037fb390f6949c2 |
| 2.0.8   | #2122061137 | https://www.virustotal.com/gui/file/f170b46d0e5472722e5b9942e58aab5d5ea9470185916d9c35d9388c41f71a5b |

- APKLab Analysis

| Release | Build       | APKLab Report                                                                                    |
|---------|-------------|--------------------------------------------------------------------------------------------------|
| 2.0.0   | #2110141448 | https://apklab.io/apk.html?hash=92d2a29449b779fae9873aec0cff1d84d98a1919f9af1fc9cd841d7371681778 |
| 2.0.1   | #2110270441 | https://apklab.io/apk.html?hash=a8caba9e40180cd2b2d4be5f36edaeaeaadf64c21937fe648fc5e0f050f7686a |
| 2.0.2   | #2111271430 | https://apklab.io/apk.html?hash=2204ccaf37dc700ab071c8a3f2d1209cc3bca9b50b95793cb9189a0e7508453e |
| 2.0.3   | #2112231914 | https://apklab.io/apk.html?hash=684fbb9ab0bcd388f55d2d474d0d795a0ae1ecafd616028fe2b1ac8a658039ff |
| 2.0.4   | #2112281840 | https://apklab.io/apk.html?hash=7284aa5e6346d3061a86fa34e63c7b2d211c3deedc5d10f985ba702cde68fac1 |
| 2.0.6   | #2122046184 | https://apklab.io/apk.html?hash=8d4ecd242769bda2e0f828994fa8db5bd569b1f31c0cfd27e2348633b06f9845 |
| 2.0.7   | #2122054203 | https://apklab.io/apk.html?hash=9d421d883c695c41bfff3cc7ae35d5de047e3c83057ce607b037fb390f6949c2 |
| 2.0.8   | #2122061137 | https://apklab.io/apk.html?hash=f170b46d0e5472722e5b9942e58aab5d5ea9470185916d9c35d9388c41f71a5b |

- Pithus Analysis (_stripped duplicated libs - because of engine upload limits_)

| Release | Build       | Pithus Report (stripped)                                                                        |
|---------|-------------|-------------------------------------------------------------------------------------------------|
| 2.0.0   | #2110141448 | https://beta.pithus.org/report/af7a71c32e93fb152ceff7c34e12f4e7e4e6268929803cb85340198c19e287b7 |
| 2.0.1   | #2110270441 | https://beta.pithus.org/report/0a9196612c761d7009c730924070137f6ad69ece287b2f8270b62d46d46b1e8c |
| 2.0.2   | #2111271430 | https://beta.pithus.org/report/fa60779237c3cdf44cc4a38b9e26120dafe5cf4da9460871768f13c1be4315f3 |
| 2.0.3   | #2112231914 | https://beta.pithus.org/report/17da25e6ecefbd64c0472df0aa2c4f4c53e48f7436a61e32ccf6191e2611b39f |
| 2.0.4   | #2112281840 | https://beta.pithus.org/report/9777bfc8a53949b66ecf0a7355aa40577e089086a8c65977ae3801efa9456124 |
| 2.0.6   | #2122046184 | https://beta.pithus.org/report/067cd4cfc165c740e2a6fbf8919f691c8bcb9abed2a8b786f33f109dc7660ca4 |
| 2.0.7   | #2122054203 | https://beta.pithus.org/report/d5462ccd3bc9e66270c38cf1cfc8d683e26154966cbd4b9e82b822458396167b |
| 2.0.8   | #2122061137 | https://beta.pithus.org/report/f9b6cb67a92787892c67ae06d588cef955b9bc54f31e6293c0f265ac7d0cb522 |

- JoeSandbox Analysis (_stripped duplicated libs - because of engine upload limits_)

| Release | Build       | JoeSandbox Report (stripped)                      |
|---------|-------------|---------------------------------------------------|
| 2.0.0   | #2110141448 | https://www.joesandbox.com/analysis/574166/1/html |
| 2.0.1   | #2110270441 | https://www.joesandbox.com/analysis/566751/0/html |
| 2.0.2   | #2111271430 | https://www.joesandbox.com/analysis/574171/1/html |
| 2.0.3   | #2112231914 | https://www.joesandbox.com/analysis/574201/1/html |
| 2.0.4   | #2112281840 | https://www.joesandbox.com/analysis/574203/0/html |
| 2.0.6   | #2122046184 | https://www.joesandbox.com/analysis/574207/0/html |
| 2.0.7   | #2122054203 | https://www.joesandbox.com/analysis/562596/0/html |
| 2.0.8   | #2122061137 | https://www.joesandbox.com/analysis/564312/0/html |

\
Let's compare such previous versions of 2.0.x for:

- Supported ARM architecture
- Agora RTC SDK dependency
- iFlyTek AIUI SDK dependency
- SensorsData local DB
- Declared Permissions + Activities + Services + Receivers

| Rel. | Build       | arm | arm-v7a | arm64-v8a | Agora RTC | iFlyTek AIUI | SensorsData | PRM | ACT | SRV | RCV| 
|------|-------------|-----|---------|-----------|-----------|--------------|-------------|-----|-----|-----|----|
| 2.0.0| #2110141448 | ✅  | ✅     | ❌       | ✅        | ✅          | ✅          | 67  | 205 | 20  | 19 |
| 2.0.1| #2110270441 | ✅  | ✅     | ❌       | ✅        | ✅          | ✅          | 67  | 205 | 20  | 19 |  
| 2.0.2| #2111271430 | ✅  | ✅     | ❌       | ✅        | ✅          | ✅          | 67  | 205 | 20  | 19 |   
| 2.0.3| #2112231914 | ✅  | ✅     | ❌       | ✅        | ✅          | ✅          | 67  | 206 | 20  | 19 |  
| 2.0.4| #2112281840 | ✅  | ✅     | ❌       | ✅        | ✅          | ✅          | 67  | 206 | 20  | 19 |  
| 2.0.6| #2122046184 | ❌  | ✅     | ✅       | ✅        | ✅          | ✅          | 67  | 206 | 20  | 19 | 
| 2.0.7| #2122054203 | ❌  | ✅     | ✅       | ❌        | ✅          | ✅          | 64  | 193 | 15  | 16 |

\
As You can see, the real app changes occur since release 2.0.7 = drop of Agora RTC SDK & declarated perms / activities / services / receivers rearrangement. There aren't indeed any significant changes up to release 2.0.6 : 

- release 2.0.3 just included a slight refactoring of the activities, but without any functional changes = check the activity comparison between the releases

- release 2.0.6 dropped the ARCH armeabi support (_remnant of Systoon sibling apps' deprecated code_) and finally added the support for ARCH arm64-v8a in the Universal APK builds. Until then only MY 2022 release 2.0.2 & 2.0.4 PlayStore AAB builds supported ARCH arm64-v8a (_because of Google requirements_). However, arm64 libs dependencies & implementation were the same of the armv7 ones = no functional differences

\
Therefore, it is not surprising at all to run any 2.0.x release up to 2.0.6 & find exactly the same runtime situation as in version 2.0.1.

You get indeed the same UI outputs / created files & folders / network traffic / raw logs / etc., that I've already provided about the execution of release 2.0.1 = [Appendix](#7-appendix).

Anyway, since some users would like to cross-check my findings with those of the quoted third-party reports, I've also uploaded the runtime raw logs of releases 2.0.4 & 2.0.6 in the [appendix folder](appendix). Such logs have been collected while running those builds in full Normal Perms + Special Perms mode = manually allowed system-write + app-overlay perms too.

\
In the end, even this further appendix doesn't change obviously my [conclusions](#6-conclusions) about past & current MY 2022 2.0.x releases for Android.

**Regardless of the (current or past) MY 2022 build you're checking, MY 2022 isn't a sophisticated spyware, but just a poorly developed app with its obvious bugs & security issues (_i.e archived logs/appdata into the internal shared storage_).**

**Poor development owing to the recycling of deprecated code (_just check the other TOON sibling-apps_) & to the resulting persistence of questionable project settings, such as keeping minSDK API Level 21 (Lollipop 5.0 - 1st Release October 2014)**.
